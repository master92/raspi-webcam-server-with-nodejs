on_raspi = true

fs = require('fs')
http = require('http')
request = require('request')
url = require('url')

express = require("express")
app = express()

sys = require('sys')
exec = require('child_process').exec


fs.writeFile "cam_on","0",(err) ->
  app.get '/image/:on', (request, response) ->
    lighton = request.params.on
    response.writeHead(200, {'Content-Type': 'image/jpg'})
    if(lighton == "on")
      exec "433/send 23 0 0 0 0 0 1 0 0 0 0 1", ->
      console.log("turned light on")
    cam_on ->
      if(on_raspi)
        use_cam((data) ->
          response.end(data)
          if(lighton == "on")
            exec "433/send 23 0 0 0 0 0 1 0 0 0 0 0", ->
            console.log("turned light off")
          return
        );
      else
        timestamp = new Date().getTime()
        exec "gnome-screenshot -f images#{timestamp}.jpg", (error, stdout, stderr) ->
          file = fs.readFile "images/#{timestamp}.jpg",(err,data) ->
            response.end(data)
            return
          return
      return
    return

  app.get '/', (request, response) ->
    response.sendfile(__dirname + "/index.html")
    return

  app.get '/loading', (request, response) ->
    response.sendfile(__dirname + "/loading.gif")
    return

  app.get '/shortcut', (request, response) ->
    response.sendfile(__dirname + "/pi_icon.png")
    return

  app.get '/get_x_pos', (request, response) ->
    response.sendfile(__dirname + "/cam_pos_x")
    return

  app.get '/get_y_pos', (request, response) ->
    response.sendfile(__dirname + "/cam_pos_y")
    return

  app.get '/lighton', (request, response) ->
    exec "433/send 23 0 0 0 0 0 1 0 0 0 0 1", ->
      console.log("turned light on")
      return
    response.end()
    return

  app.get '/lightoff', (request, response) ->
    exec "433/send 23 0 0 0 0 0 1 0 0 0 0 0", ->
      console.log("turned light off")
      return
    response.end()
    return


  app.get '/move_x/:value_x', (request, response) ->
    value_x = request.params.value_x/256
    value_x2 = Math.round(value_x*2000)
    console.log("moving x to "+request.params.value_x+"...")
    fs.readFile 'cam_pos_x',(err, content) ->
      exec "softpwm/pwm 18 "+Math.abs(content-request.params.value_x)+" "+value_x2+" "+(20000-value_x2), ->
        console.log("moved x!")
        fs.writeFile("cam_pos_x",request.params.value_x)
        response.end()
        return
      return
    return

  app.get '/move_y/:value_y', (request, response) ->
    value_y = request.params.value_y/256
    value_y2 = Math.round(value_y*2000)
    console.log("moving y to "+request.params.value_y+"...")
    fs.readFile 'cam_pos_y',(err, content) ->
      exec "softpwm/pwm 15 "+ Math.abs(content-request.params.value_y)+" "+value_y2+" "+(20000-value_y2),->
        console.log("moved y!")
        fs.writeFile("cam_pos_y",request.params.value_y)
        response.end()
        return
      return
    return

  app.listen(8080)
  console.log "Server is running..."
  return

#this functions looks if cam is in use or not. If it is in use it gives a callback when it is finished
cam_on = (callback) ->
  fs.readFile 'cam_on',(err, content) ->
    if(`content == "1"`)
      console.log("someone else is using the cam...");
      setTimeout ->
        cam_on ->
          console.log("cam now free")
          return
        return
      ,500
    else
      callback()
    return
  return

use_cam = (callback) ->
  fs.writeFile "cam_on","1",(err) ->
    console.log("Camera is in use now!")
    timestamp = new Date().getTime()
    exec "raspistill -hf -vf -o images/"+timestamp+".jpg -w 640 -h 480 -q 70", (error, stdout, stderr) ->
      fs.writeFile "cam_on", "0",(error) ->
        console.log("Camera is free to use again")
        if(error)
          console.log("Something went wrong")
        else
          console.log("Success!")
        file = fs.readFile 'images/'+timestamp+'.jpg',(err,data) ->
          callback(data);
          return
        return
      return
    return
  return