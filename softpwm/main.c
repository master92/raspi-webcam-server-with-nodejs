/* fopen1.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
   //  Argumente: Pin Numer; totaltime, uptime, lowtime
   if (argc != 5) {
      printf("Erwarte 4 Argumente, %d gegeben. ", argc);
      printf("Argumente: Pin Number - Totalzeit[ms] - TimeOn[us] - TimeOff[us]");
      exit(0);
   }

   int i = 0;
   int timegone = 0;
   char cmd1[200];
   char cmd2[200];
   char cmd3[200];
   char file[200];
   FILE *datei;


   sprintf(cmd1, "echo %s > /sys/class/gpio/export", argv[1]);
   sprintf(cmd2, "echo out > /sys/class/gpio/gpio%s/direction", argv[1]);
   sprintf(cmd3, "echo 0 > /sys/class/gpio/gpio%s/value", argv[1]);
   sprintf(file, "/sys/class/gpio/gpio%s/value", argv[1]);
   system(cmd1);
   system(cmd2);
   system(cmd3);

//   sprintf(file, "/home/pi/test.txt");

   datei = fopen(file, "w");
   if(NULL == datei) {
      printf("Konnte Datei %s nicht öffnen!\n", file);
      return EXIT_FAILURE;
   } else {
     printf("Datei zum schreiben geöffnet\n");
     while(1) {
       if (timegone > ((atoi(argv[2]))*1000)) {
          break;
       }
       putc(49, datei);
       fflush(datei);
       usleep(atoi(argv[3]));
//       printf("1");
       putc(48, datei);
       fflush(datei);
       fflush(stdout);
       usleep(atoi(argv[4]));
//       printf("0");
       fflush(stdout);
       timegone = timegone +atoi(argv[3]) +atoi(argv[4]);
     }
     printf("\n");
   }
   return EXIT_SUCCESS;
}
