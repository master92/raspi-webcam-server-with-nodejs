/* fopen1.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int small_time = 250;
int large_time = 780;

FILE *datei;

void sendshort(){
  putc(49, datei);
  fflush(datei);
  usleep(small_time);
  putc(48, datei);
  fflush(datei);
  usleep(large_time);
}

void sendlong(){
  putc(49, datei);
  fflush(datei);
  usleep(large_time);
  putc(48, datei);
  fflush(datei);
  usleep(small_time);
}

int main(int argc, char *argv[]) {
   //  Argumente: Pin Nummer; 10 signals, on or off
   if (argc != 13) {
      printf("Erwarte 13 Argumente, %d gegeben. ", argc);
      printf("Argumente: Pin Number - 10 signale - on or off");
      exit(0);
   }
   int counter = 2;
   int i = 0;
   int timegone = 0;
   char cmd1[200];
   char cmd2[200];
   char cmd3[200];
   char file[200];

   sprintf(cmd1, "echo %s > /sys/class/gpio/export", argv[1]);
   sprintf(cmd2, "echo out > /sys/class/gpio/gpio%s/direction", argv[1]);
   sprintf(cmd3, "echo 0 > /sys/class/gpio/gpio%s/value", argv[1]);
   sprintf(file, "/sys/class/gpio/gpio%s/value", argv[1]);
   system(cmd1);
   system(cmd2);
   system(cmd3);

   datei = fopen(file, "w");
   if(NULL == datei) {
      printf("Konnte Datei %s nicht öffnen!\n", file);
      return EXIT_FAILURE;
   } else {
     printf("Datei zum schreiben geöffnet\n");
     while(i < 100){
       //spacer
       putc(49, datei);
       fflush(datei);
       usleep(small_time);
       putc(48, datei);
       fflush(datei);
       usleep(large_time);
       while(counter <= 11) {
         if(atoi(argv[counter]) == 0){
           sendlong();
           sendshort();
         }else{
           sendshort();
           sendshort();
         }
         counter++;
       }
       counter = 2;
       if(atoi(argv[12]) == 1){
         sendshort();
         sendshort();
         sendlong();
       }else{
         sendlong();
         sendshort();
         sendshort();
       }
       //last spacer (short)
       putc(49, datei);
       fflush(datei);
       usleep(small_time);
       putc(48, datei);
       fflush(datei);
       //usleep(large_time);
       i++;
       usleep(7750);
     }
     printf("\n");
   }
   return EXIT_SUCCESS;
}


