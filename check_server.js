var fs = require('fs');

var exec = require('child_process').exec;

var request = require('request');

server_restarter(1);

function server_restarter(counter){
  request.get('http://localhost:8080/get_x_pos', function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log("server running");
    }else{
      console.log("server not running: "+error);
      console.log("restarting server...");
      exec("/opt/node/node getpic.js", function (error, stdout, stderr) {     
        console.log('server crashed '+counter+' times now!');
        server_restarter(counter+1);
      });
      console.log("server restarted!");
    }
  });
}
